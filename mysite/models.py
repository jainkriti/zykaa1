from django.db import models 
from django.contrib.auth.models import User

class customer(models.Model): 
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact = models.IntegerField()
    city = models.CharField(max_length=200)
    registred_on = models.DateTimeField(auto_now_add=True)
    changed_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username

class delivery_partner(models.Model):
    gender_choices = (
        ('Mr.','Mr.'),
        ('Miss','Miss')
    )
    subtitle = models.CharField(choices=gender_choices,max_length=100)
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    profile_pic = models.FileField(upload_to='profiles/%Y/%m/%d',blank=True)
    city = models.CharField(null=True,max_length=200)
    age = models.IntegerField()
    contact = models.IntegerField()
    is_available = models.BooleanField(default=True)
    approved = models.BooleanField(default=False,null=True)
    registered_on = models.DateTimeField(auto_now_add = True)
    updated_on = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.user.username

class city(models.Model):
    name =models.CharField(max_length=200)
    zip_code = models.CharField(max_length=100)
    added_on = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name

class Restaurant(models.Model):
    email = models.EmailField()
    name = models.CharField(max_length=200)
    RES_TYPE = (
        ('Bar','Bar'),
        ('Restaurant','Restaurant'),
        ('Cafe','Cafe'),
        ('Bakery','Bakery')
    )
    res_type = models.CharField(max_length=10,choices = RES_TYPE,default = 'R')
    cuisine = models.CharField(null = True, max_length=100)
    city = models.ForeignKey(city,max_length = 100,null = True,on_delete=models.CASCADE)
    address = models.TextField()
    phone = models.IntegerField(blank = True)
    image = models.ImageField(default = '/static/images/img/gallery.jpg',upload_to = 'media/%Y/%m/%d')
    open_at = models.TimeField()
    closes_at = models.TimeField()
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class category(models.Model):
    name = models.CharField(max_length=200,unique=True)
    added_on = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(null=True,default = '/static/images/img/gallery.jpg',upload_to = 'media/%Y/%m/%d')
    def __str__(self):
        return self.name

class food_item(models.Model):
    CH = (('Veg','Veg'),('Non-Veg','Non-Veg'))
    COURSE = (
        ('Starter','Starter'),
        ('Main Course','Main Course'),
        ('Desert','Desert')
    )
    course = models.CharField(max_length=100,choices=COURSE)
    rest_id = models.ForeignKey(Restaurant,on_delete = models.CASCADE)
    item_name = models.CharField(max_length=200)
    category_id = models.ForeignKey(category,max_length=200,on_delete=models.CASCADE)
    ingredients = models.TextField(blank=True)
    price = models.IntegerField()
    type = models.CharField(choices=CH,max_length=100)
    status = models.BooleanField()
    preparation_time = models.TimeField()
    added_on = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(null=True,default = '/static/images/img/gallery.jpg',upload_to = 'media/%Y/%m/%d')
    def __str__(self):
        return self.item_name

class cart(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    product_id = models.ForeignKey(food_item, on_delete=models.CASCADE,null=True)
    quantity = models.IntegerField()
    status = models.BooleanField(default=False)
    type=models.IntegerField(null=True,blank=True,default=0)
    create_date = models.DateTimeField(auto_now_add = True)
    updated_on = models.DateTimeField(auto_now = True)

    def __str__(self):
        return str(self.id)

class order(models.Model):
    ORDER = (('Delivered','Delivered'),('Pending','Pending'))
    customer = models.ForeignKey(customer, on_delete=models.CASCADE)
    assigned_to = models.ForeignKey(delivery_partner, on_delete=models.CASCADE,blank=True,null=True)
    amount = models.DecimalField(max_digits=20,decimal_places=2)
    contact_name = models.CharField(max_length=250)
    contact_number = models.CharField(max_length=250)
    contact_email = models.CharField(max_length=250) 
    delivery_address = models.CharField(max_length=250)
    txn_id = models.CharField(max_length=250,blank=True)
    payment_mode = models.CharField(max_length=100,blank=True)
    bank_name = models.CharField(max_length=200,blank=True)
    status = models.CharField(max_length=200,blank=True,choices=ORDER,default="Pending")
    cart_id = models.CharField(max_length=500,null=True)
    received_on = models.DateTimeField(auto_now_add=True)
    changed_on = models.DateTimeField(auto_now_add=True,null=True)
    delivered_on = models.CharField(max_length=20,null=True,blank=True)
    def __str__(self):
        return self.customer.user.username

class order_details(models.Model):
    user_id = models.ForeignKey(User,on_delete = models.CASCADE,blank=True,null=True)
    order_id = models.ForeignKey(order,on_delete = models.CASCADE,blank=True,null=True)
    cart_id = models.ForeignKey(cart,on_delete = models.CASCADE,blank=True,null=True)
    delivery_boy = models.ForeignKey(delivery_partner,on_delete = models.CASCADE,blank=True,null=True)
    status = models.BooleanField(default=False)
    added_on = models.DateTimeField(auto_now_add=True)
    changed_on = models.DateTimeField(auto_now_add=True,null=True)
    def __str__(self):
        return self.user.name

class feedback(models.Model):
    name = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    phone = models.IntegerField()
    message = models.TextField()
    create_date = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.message
